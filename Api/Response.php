<?php


namespace Api;

use Api\ResponseInterface;
use Api\HttpStatusCode;

class Response implements ResponseInterface
{
    /**
     * Status code
     *
     * @var int
     */
    protected $status = HttpStatusCode::HTTP_OK;

    /**
     * Content
     *
     * @var array
     */
    protected $content;

    /**
     * @param array $data
     * @return Response
     */
    public function setContent(array $data): Response
    {
        $this->content = $data;
        return $this;
    }

    /**
     * @param \Api\HttpStatusCode $code
     * @return Response
     */
    public function setHttpCode(HttpStatusCode $code): Response
    {
        $this->status = $code;
        return $this;
    }

    /**
     * @param bool $jsonEncoded
     * @return array|false|mixed|string
     */
    public function getContent(bool $jsonEncoded = true)
    {
        return ($jsonEncoded === true) ? json_encode($this->content) : $this->content;
    }

    /**
     * @return int
     */
    public function getHttpCode(): int
    {
        return $this->status;
    }
}