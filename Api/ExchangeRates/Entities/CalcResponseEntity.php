<?php

namespace Api\ExchangeRates\Entities;

use Api\ExchangeRates\Entities\ResponseEntity;

class CalcResponseEntity extends ResponseEntity
{
    /**
     * @var string
     */
    private $resultingRate = "0.00000";

    /**
     * @var string
     */
    private $amount = "0";

    /**
     * @return string
     */
    public function getResultingRate(): string
    {
        return $this->resultingRate;
    }

    /**
     * @param string $resultingRate
     * @return CalcResponseEntity
     */
    public function setResultingRate($resultingRate): CalcResponseEntity
    {
        $this->resultingRate = $resultingRate;
        return $this;
    }

    /**
     * @return string
     */
    public function getAmount(): string
    {
        return $this->amount;
    }

    /**
     * @param string $amount
     * @return CalcResponseEntity
     */
    public function setAmount($amount): CalcResponseEntity
    {
        $this->amount = $amount;
        return $this;
    }

    public function toArray(): array
    {
        return [
            'resultingRate' => $this->resultingRate,
            'amount' => $this->amount
        ];
    }



}