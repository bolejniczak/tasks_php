<?php

namespace Api\ExchangeRates\Entities;

abstract class ResponseEntity
{
    /**
     * @return string
     */
    public function toJson(): string {
        return json_encode($this->toArray());
    }

    /**
     * @return array
     */
    abstract public function toArray(): array;
}