<?php

namespace Api\ExchangeRates;

class Assert
{
    /**
     * @param string $key
     * @param array $input
     * @throws \Exception
     */
    public static function inArray(string $key, array $input)
    {
        if (!array_key_exists($key, $input)) {
            throw new \Exception('Key ' . $key . ' is not in a input table');
        }
    }

    /**
     * @param $var
     * @throws \Exception
     */
    public static function isString($var)
    {
        if (!is_string($var)) {
            throw new \Exception('Argument is not a string');
        }
    }
}