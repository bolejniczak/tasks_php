<?php
declare(strict_types=1);

namespace Api\ExchangeRates;

use Api\ExchangeRates\ {
    Assert,
    Entities\CalcResponseEntity,
    Services\CurrentExchangeRateService
};
use Api\Response;
use Api\ResponseInterface;

class CalculatorController
{
    protected $config;

    public function __construct()
    {
        // config loader should be some kind of Singleton pattern
        $this->config = require (__DIR__ . '/../../config/global.php');
    }

    /**
     * @param array $inputArray assume any input array (like $_GET)
     * @return ResponseInterface
     */
    public function calculateCurrentExchangeRateAction(array $inputArray = []): ResponseInterface
    {
        Assert::inArray('fromCurrency', $inputArray);
        Assert::inArray('toCurrency', $inputArray);
        $currentExchangeRateService = new CurrentExchangeRateService($this->config['apisEndpoints']['nbp']['exchangeRatesTableA']);
        $resultingRate = $currentExchangeRateService->getResultingRate($inputArray['fromCurrency'], $inputArray['toCurrency']);
        $responseEntity = new CalcResponseEntity();
        $responseEntity->setResultingRate(number_format($resultingRate, 5));
        if (array_key_exists('amount', $inputArray)) {
            $responseEntity->setAmount(
                number_format(
                    $currentExchangeRateService->getAmount($resultingRate, $inputArray['amount']),
                    5
                )
            );
        }
        $response = new Response();
        return $response->setContent($responseEntity->toArray());
    }

    /**
     * @param array $inputArray assume any input array (like $_GET)
     * @return ResponseInterface
     */
    public function getRateStatsAction(array $inputArray = []): ResponseInterface
    {

    }

}