<?php

namespace Api\ExchangeRates\Services;

use Symfony\Component\HttpClient\CurlHttpClient;

class CurrentExchangeRateService
{
    private $apiEndpoint = '';

    public function __construct(string $apiEndpoint)
    {
        $this->apiEndpoint = $apiEndpoint;
    }

    /**
     * @return array
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    private function getData(): array
    {
        $curl = new CurlHttpClient();
        return $curl
            ->request('GET', $this->apiEndpoint)
            ->toArray();
    }

    /**
     * @param string $fromCurrencyCode
     * @param string $toCurrencyCode
     * @return float
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function getResultingRate(string $fromCurrencyCode, string $toCurrencyCode): float
    {
        $fullTableRates = $this->getData();
        array_walk(
            $fullTableRates[array_key_first($fullTableRates)]['rates'],
            function($currency) use (&$filteredRates, $fromCurrencyCode, $toCurrencyCode){
                if($currency['code'] === $fromCurrencyCode || $currency['code'] === $toCurrencyCode){
                    $filteredRates[$currency['code']] = $currency['mid'];
                }
            }
        );
        return $filteredRates[$fromCurrencyCode] / $filteredRates[$toCurrencyCode];
    }

    /**
     * @param float $resultingRate
     * @param int $amount
     * @return float
     */
    public function getAmount(float $resultingRate, int $amount): float
    {
        return $resultingRate * $amount;
    }

}