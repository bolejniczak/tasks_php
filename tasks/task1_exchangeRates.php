<?php

require_once 'bootstrap.php';

/**
 * ====== PLEASE PROVIDE THE FOLLOWING: =======
 * Development Time Estimate [in hours]:    5h
 * Actual Development Time [in hours]:      min-code version for POC purpose: about 30min
 *                                          industry version: about 3h:
 *                                              got to recall some features and double check if it's php7.2 compatible
 *                                              and overview the provided Symphony vendors
 * Your thoughts and comments:              1. First and foremost:
 *                                              Extra challenge (optional): assume heavy load (many requests per second)
 *                                              This can be dealt with caching solution available from provided Symphony vendors
 *                                              i.e.
 *                                              $store = new Store('/path/to/cache/storage/');
 *                                              $client = HttpClient::create();
 *                                              $client = new CachingHttpClient($client, $store);
 *                                              $response = $client->request('GET', 'http://api.nbp.pl/api/exchangerates/tables/a/');
 *                                          2. Regarding task requirement:
 *                                              Resulting rates should have exactly 5 decimal places (no more, no less)
 *                                              Now, if it HAS TO BE exactly 5 decimal places than number_format() is a function to use.
 *                                              This has a downside - the output is a string, not a float.
 *                                              On the other side - there is a round() function that returns floats, but cannot prevail '0' at the end
 *                                              therefore it trims lasting '0' i.e. 3.45600 will be 3.456
 *                                          3. Application configuration should be some kind of Singleton pattern
 * ============================================
 */

/**
 * correct input EXAMPLE - it may be modified (and will be modified during task assessment)
 */
$_GET = [
    'fromCurrency' => 'USD',
    'toCurrency' => 'GBP',
    'amount' => 50,
];

$controller = new \Api\ExchangeRates\CalculatorController();
$response = $controller->calculateCurrentExchangeRateAction($_GET);


// print the returned response
http_response_code($response->getHttpCode());
header('Content-type: application/json');
echo $response->getContent(true);
